class Graph():
    """
    Undirected, unweighted graph class
    """

    def __init__(self):
        self._vertices = {}                                                     # dict with vertices as keys, values associated with keys are the "heads" of edges incident to each key vertex
        self._edges = set()                                                     # list of tuples, tail of edge is in position 0 and head is in position 1. Using set for better performance

    def populate_graph(self, di):
        """
        adds edges and vertices to graph G from dictionary DI where keys are
        vertices and the vertices in the list object are the adjacent vertices
        """
        if len(di) == 0:
            print("Empty Dictionary")
        while len(di) > 0:
            vertex, adj_vertices = di.popitem()
            if vertex not in self._vertices:
                self.add_vertex(vertex)
                for v in adj_vertices:
                    self.add_edge((vertex, v))
                    self.add_adj_vertices(vertex, v)

    def add_edge(self, e):
        """
        adds undirected edge
        """
        self._edges.add(min(e[0], e[1]), max(e[0],e[1]))                        # doing min and max so that no parallel edges

    def add_vertex(self, v):
        """
        adds vertex to undirected graph
        """
        self._vertices[v] = set()

    def add_adj_vertices(self, vertex, adj_v):
        """
        adding all adjacent vertices to set object to associated key VERTEX
        """
        self._vertices[vertex].add(adj_v)

    def get_vertex(self, v):
        """
        returns value associated with key V in the vertices dictionary
        """
        return self._vertices[v]

    @property
    def num_vertices(self):
        return len(self.vertices)

    @property
    def num_edges(self):
        return len(self._edges)

    @property
    def vertices(self):
        return list(self._vertices)

    @property
    def tails_heads(self):
        """
        returns the dictionary keys and values of vertices of graph
        """
        return self._vertices

    @property
    def edges(self):
        return self._edges


class diGraph(Graph):
    """
    Unweighted digraph class
    """


    def populate_graph(self, di):
        """
        adds edges and vertices to graph G from a dictionary, DI, where keys are
        vertices and elements in its list object are the heads of each
        directed edge incident to that key vertex
        if graph not empty adds to current graph, checks to ensure keys are same
        type to minimize mixing of 2 incompatible
        """
        if len(di) == 0:
            print("Empty Dictionary")
        while len(di) > 0:
            _tail, _heads = di.popitem()
            if _tail not in self._vertices:                                     # don't use self._vertices.keys() as this will slow down code as that creates a list
                self.add_vertex(_tail)
            for i in _heads:
                if i not in self._vertices:
                    self.add_vertex(i)
                self.add_edge((_tail,i))


    def add_edge(self, e):
        """
        adds edge E to _EDGES list. E must be incident to already existing
        vertices found in _VERTICES dictionary
        """
        assert isinstance(e, tuple), "edge not represented as a tuple"
        assert len(e) == 2, "incorrect number of vertices in this edge"
        assert e not in self._edges, "edge already exists"
        assert e[0] in self._vertices and e[1] in self._vertices, \
        "One or more vertices of edge not in graph"
        self._edges.add(e)
        self.add_head(e)
        self.add_tail(e)

    def add_vertex(self, v):
        assert v not in self._vertices, "vertex already exists"
        #if len(self._vertices) > 0:
            #assert isinstance(v, type(list(self._vertices.keys())[0])), \      # took out as doesn't matter if keys are different object types, can add an enumerate function that labels all with numbers to avoid that problem
            #"not same type as other vertices"
        self._vertices[v] = (set(), set())                                      # first set object in tuple is the set of head vertices associated with the key vertex, which is the tail. And 2nd element is the set of tail vertices with the key vertex as the head

    def add_head(self, e):
        """
        takes a new edge E, and adds head vertex to set object associated
        with the tail vertex in E
        """
        self._vertices[e[0]][0].add(e[1])

    def add_tail(self,e):
        """
        takes new edge E and adds tail vertex to set object associated with the
        head vertex in E
        """
        self._vertices[e[1]][1].add(e[0])

    def trav_field(self):
        pass

    def leader_field(self):
        self._leader = {}

    def assign_leader(self, le, v):
        if le in self._leader:
            self._leader[le].append(v)
        else:
            self._leader[le] = [v]


    def finish_field(self):
        self._flist = []

    def finish_time(self, v):
        self._flist.append(v)

    def heads(self, v):
        return self.get_vertex(v)[0]

    def tails(self, v):
        return self.get_vertex(v)[1]

    def get_head(self,v):
        """
        returns an arbitrary head or returns default value of None
        """
        try:
            head = self._vertices[v][0].pop()
        except KeyError:
            return None
        self._vertices[v][0].add(head)
        return head



    """
    @top_sort.setter
    def top_sort(self):
        self._top_sort = list(self.finish_list)
        self._top_sort.reverse()

    @property
    def top_sort(self):
        return self._top_sort
    """

    @property
    def SCC_num(self):
        assert self._leader and self._flist, 'Need to run K2P Algorithm first'
        return len(self._leader)

    @property
    def SCC_groups(self):
        assert self._leader and self._flist, 'Need to run K2P Algorithm first'
        return list(self._leader.values())

    @property
    def SCC_sizes(self):
        assert self._leader and self._flist, 'Need to run K2P Algorithm first'
        self._SCCsizes = []
        for i in self.SCC_groups:
            self._SCCsizes.append(len(i))
        return self._SCCsizes

    @property
    def finish_list(self):
        return self._flist




class weighted_Graph(Graph):
    """
    weighted undirected graph class
    """

    def populate_graph(self, di):
        """
        populates weighted graph with dictionary DI. DI should have vertices as
        keys and sets of tuples that have their first element as the adj vertex
        to the key and the weight value of the edge incident to those 2 vertices
        """
        if len(di) == 0:
            print("Empty Dictionary")
        else:
            while di:
                vertex, adj_vertices = di.popitem()
                if vertex not in self._vertices:
                    self.add_vertex(vertex)
                    for head, weight in adj_vertices:
                        self.add_edge((weight,vertex,head))
                        self.add_adj_vertices(vertex, (weight, vertex, head))
                #currently assuming that value associated with keys are some sort of iterable with 3 element tuple elements (1) weight of edge (2) tail (key) (3) head


    def add_edge(self, e):
        """
        adding weighted edge
        """
        self._edges.add(e)




