import re
import os

def file_to_dict_weighted(path):
    """
    reads txt file and returns a dictionary object DI. 1st number on each line
    of text file is a key, while all subsequent numbers on line elements in
    that key's list object
    """
    di = {}
    with open(path, mode='r') as a_file:
        for line in a_file:
            data = re.split('\s|\t|\n', line)
            #print(data)
            if int(data[0]) not in di:
                di[int(data[0])] = []

            for i in data[1:]:
                if i:
                    di[int(data[0])].append((int(i.split(',')[0]), int(i.split(',')[1])))


    return di

f = input("Which file to read?")
subpath = os.getcwd()
path = os.path.join(subpath,f)


def file_to_lst(path):
    lst = []
    with open(path, mode='r') as a_file:
        for num in a_file:
            lst.append(int(num))
    return lst

def file_to_set(path):
    """
    takes file and puts into a set
    """
    s = set()
    with open(path, mode='r') as a_file:
        for line in a_file:
            data = line.split()
            for i in data:
                s.add(int(i))
    return s

"""
#ensuring the tuple elements are int types
#d = file_to_dict_weighted(os.path.join(path,f))
print(type(d[1][1][1]))
"""
#testing if reading the dijkstra text file correctly

def dict_to_file(path, di):
    """
    write dictionary to file
    """
    output = os.path.join(path, "Dijkstra_Output.txt")
    with open(output, mode='w') as a_file:
        for i in di:
            #print(type(i))
            a_file.write(str(i) + ":" + str(di[i]))
            a_file.write("\n")
