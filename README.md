# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Purpose: Practice implementation algorithms / creating functions.
Implementation of Dijkstra's shortest path algorithm.
Does not optimize performance with heaps (to come later).

### How do I get set up? ###
Download repository and run "python3 dijkstra.py" on command line. Then enter the "file.txt" (file.txt must be in same directory as the dijkstra.py file). 
The input file is a graph object. With the first number 'v' is a vertex label - followed by a pair of numbers (separated by a comma) x,y where x = an adjacent vertex to 'v' and 'y' is the weight of this directed edge.
Outputs a file with the vertices as the first number of each line and the shortest path from the SOURCE vertex to the stated vertex on the line. If no path then outputs 1000000000000000