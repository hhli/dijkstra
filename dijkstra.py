import os
from GraphObj import *
import heapq
import readfiles


"""
def dijkstra(graph, source):

    dijkstra algorithm of shortest path. Takes in a GRAPH and computes shortest
    paths from SOURCE to all vertices with a path from SOURCE.
    Implementation with heaps. TO BE CONTINUED

    negative weights not allowed

    processed = {}                                                              # making this a dictionary to make for easy retrieval of shortest path of each vertex connected to SOURCE
    frontier = []                                                               # list to be transformed into a heap via heapq module. Making sure to make a new object so not to mistakenly change graph attributes. Represents the edges whose tails are in PROCESSED and heads are unprocessed vertices. Contains weights and the heads
    for edge in graph.get_vertex(source):
        heapq.heappush(frontier, (edge[0], edge[2]))
    heapq.heapify(new_edges)

    score = 0                                                                   # score (as in Dijkstra score)
    processed[source] = score

    while frontier:
        score, tail, head = heapq.heappop(frontier)                             # extracts min value from frontier, min value determined by 1st element in tuple which is weight associated with edge whose tail (2nd element) is in processed and head (3rd element) not processed yet
        processed[head] = processed[tail] + score                               # assigns dijkstra score to head by taking the edge weight incident to TAIL and HEAD and adding it to dijkstra score of TAIL
        for v in graph.get_vertex(head):
            if v[2] in frontier:
                pass
"""


def dijkstra_easy(graph, source):
    """
    dijkstra algorithm without using heaps (easier to implement)

    negative weights not allowed
    """
    processed = {}                                                              # dictionary of vertices already processed (vertices with associated Dijkstra score), keys are vertices, values are Dijkstra score
    frontier = {}                                                               # dictionary of vertices that are "head" vertices adjacent to "tail" vertices in PROCESSED. Keys are vertices in unprocessed set of vertices, values are tuple (weight, tail , head (key))
    weight = 0                                                                  # initial Dijkstra score associated with SOURCE vertex
    processed[source] = weight                                                  # setting SOURCE Dijkstra score to SCORE value
    graph_vertices = set(graph.vertices)                                        # all vertices in a graph, remaining vertices in this set to be added to PROCESSED with shortest path value that denotes no path from SOURCE
    graph_vertices.remove(source)
    for edge in graph.get_vertex(source):                                       # initial populating FRONTIER with all vertices adjacent to SOURCE as initial keys
        weight, tail, head = edge
        frontier[head] = edge

    while frontier:
        d_score = 1000000000000000
        for edge in frontier:
            weight, tail, head = frontier[edge]
            #tot_score = weight + processed[tail]
            if weight < d_score:
                d_score = weight
                new_tail = tail
                new_head = head

        processed[new_head] = d_score
        graph_vertices.remove(new_head)
        del frontier[new_head]
        new_edges = graph.get_vertex(new_head)                                  # gets all outgoing edges with tail vertex of NEW_HEAD (vertex just added into PROCESSED)
        for edge in new_edges:
            weight, tail, head = edge
            if head in processed:                                               # head is in processed thus doesn't cross frontier so do not care about that edge
                pass
            elif head not in frontier:                                          # not in frontier thus no min score calculated previously so add the value in
                frontier[head] = (d_score + weight, tail, head)
            else:
                if frontier[head][0] > (weight + d_score):
                    frontier[head] = (weight + d_score, tail, head)

    for v in graph_vertices:
        processed[v] = 1000000                                                  # for any vertex V that doesn't have a path from SOURCE, set its shortest path value to 1,000,000 per coursera instructions
    return processed
"""
#testing how to access a global variable in readfiles.py
print(readfiles.d)
"""

def run():
    """
    creates a graph object from input file. Then runs Dijkstra on it and outputs the
    shortest path from SOURCE vertex.
    """
    source = 1
    graph = weighted_Graph()
    graph.populate_graph(readfiles.file_to_dict_weighted(readfiles.path))
    #print(graph.tails_heads)
    result = dijkstra_easy(graph,source)
    print(result)
    #print(type(graph.vertices[0]))
    subpath = os.getcwd()
    readfiles.dict_to_file(subpath, result)

run()






